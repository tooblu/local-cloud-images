# general, open tcp port for ssh from host
qemu-system-x86_64 \
-device e1000,netdev=net0 \
-netdev user,id=net0,hostfwd=tcp::43022-:22 \
-accel hvf \
-m 2048 \
-vga virtio \
-usb \
-device usb-tablet \
-drive file=./ubuntu-21.10-desktop-amd64.qcow2,if=virtio \
-display default,show-cursor=on

## works general run
#qemu-system-x86_64 \
#-accel hvf \
#-m 2048 \
#-vga virtio \
#-usb \
#-device usb-tablet \
#-drive file=./ubuntu-21.10-desktop-amd64.qcow2,if=virtio \
#-display default,show-cursor=on

## works, for install
#qemu-system-x86_64 \
#-accel hvf \
#-m 2048 \
#-vga std \
#-usb \
#-device usb-tablet \
#-cdrom ../isos/ubuntu-21.10-desktop-amd64.iso \
#-drive file=./ubuntu-21.10-desktop-amd64.qcow2,if=virtio \
#-display default,show-cursor=on

#-drive file=./ubuntu-21.10-desktop-amd64.qcow2,if=virtio

# -cpu host
# -smp 2  # number cpu threads

# -show-cursor \
# -device usb-tablet \
