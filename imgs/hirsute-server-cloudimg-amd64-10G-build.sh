# file exists and is directory
if ! [ -d ../cims ]; then
    mkdir ../cims
fi

# file exists and is readable
if ! [ -r ../cims/hirsute-server-cloudimg-amd64.img ]; then
    curl \
    -o ../cims/hirsute-server-cloudimg-amd64.img \
    https://cloud-images.ubuntu.com/hirsute/current/hirsute-server-cloudimg-amd64.img
fi

# create a cloud-init NoCloud Datasource (vfat or iso9660 filesystem)
# volume must be called "cidata"
# must contain files: user-data, meta-data
rm -rf ../cins/basic/seed.iso
mkisofs \
-o ../cins/basic/seed.iso \
-V cidata \
-J -rock \
../cins/basic/user-data \
../cins/basic/meta-data

rm -rf hirsute-server-cloudimg-amd64-10G.qcow2
qemu-img create \
-F qcow2 \
-o backing_file=../cims/hirsute-server-cloudimg-amd64.img \
-f qcow2 \
hirsute-server-cloudimg-amd64-10G.qcow2 \
10G
