qemu-system-x86_64  \
  -machine accel=hvf,type=q35 \
  -cpu host \
  -m 2G \
  -nographic \
  -device virtio-net-pci,netdev=net0 \
  -netdev user,id=net0,hostfwd=tcp::2222-:22 \
  -drive if=virtio,format=qcow2,file=hirsute-server-cloudimg-amd64-10G.qcow2 \
  -drive if=virtio,file=../cins/basic/seed.iso
