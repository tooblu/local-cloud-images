qemu-system-x86_64 \
-m 2048 \
-vga virtio \
-cdrom ../isos/CentOS-8.5.2111-x86_64-boot.iso \
-accel hvf \
-usb \
-drive file=./CentOS-8.5.2111-x86_64.qcow2,if=virtio

# -cpu host
# -smp 2  # number cpu threads

# -show-cursor \
# -device usb-tablet \
